name := "PrisonersDilemmaTournament"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "com.jsuereth" %% "scala-arm" % "1.4",
  "com.github.tototoshi" %% "scala-csv" % "1.2.2",
  "com.typesafe" % "config" % "1.3.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0",
  "com.typesafe.akka" %% "akka-actor" % "2.4.1",
  "com.typesafe.play" %% "play-json" % "2.4.6",
  "commons-io" % "commons-io" % "2.4"
)
