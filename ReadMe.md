# Nerdery Prisoner's Dilemma Tournament

1. Currently, the Nerdery Prisoner's Dilemma tournament will only run on Mac OS (tested on 10.11.2).
1. To build and run, [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) must be installed (tested on 1.8.0_65).
1. Put submissions into the tournament/production/submissions folder, in sub-folders that correspond with entrant usernames. (eg. tournament/production/submissions/jdoe)
1. Add entrants into tournament/production/entrants.csv. Each row is an individual entrant, in "username,discipline,command" format.
1. Run the tournament with `sbt "run production"`.
