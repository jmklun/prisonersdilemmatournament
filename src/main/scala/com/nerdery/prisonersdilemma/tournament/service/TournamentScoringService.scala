package com.nerdery.prisonersdilemma.tournament.service

import com.nerdery.prisonersdilemma.tournament.model._
import com.nerdery.prisonersdilemma.tournament.service.TournamentScoringService.{DisciplineAccumulator, EntrantAccumulator}

import scala.collection.mutable

/**
  * The TournamentScoringService contains all of the logic for the results of a tournament. It must be configured to know the number of
  * years of sentencing that each prisoner receives based on different responses.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class TournamentScoringService(bothConfessYears: Int, bothSilentYears: Int, splitConfessorYears: Int, splitSilentYears: Int) {

  /**
    * Convert the results of a tournament into a scores for entrants and disciplines.
    *
    * @param tournamentResult Completed discipline results
    * @return Ranked scores for a tournament
    */
  def scoreTournament(tournamentResult: TournamentResult): TournamentScores = {
    val rawEntrantScores = tournamentResult.matches.foldLeft(mutable.Map[Entrant, EntrantAccumulator]()) {
      case (entrantAccumulators, matchResult) =>
        accumulateEntrants(entrantAccumulators, matchResult)
    }

    val rawDisciplineScores = rawEntrantScores.foldLeft(mutable.Map[Discipline, DisciplineAccumulator]()) {
      case (disciplineAccumulators, (entrant, entrantAccumulator)) =>
        accumulateDiscipline(disciplineAccumulators, entrant, entrantAccumulator)
    }

    val interrogationsPerMatch = rawEntrantScores.headOption.fold(1)(_._2.interrogationCount)
    val entrantStatistics = convertToEntrantStatistics(rawEntrantScores)
    val disciplineStatistics = convertToDisciplineStatistics(rawDisciplineScores, interrogationsPerMatch)
    TournamentScores(entrantStatistics, disciplineStatistics)
  }

  private def convertToDisciplineStatistics(rawDisciplineScores: mutable.Map[Discipline, DisciplineAccumulator],
                                            interrogationsPerMatch: Int): Iterable[(Discipline, DisciplineStatistics)] =
    rawDisciplineScores.map {
      case (discipline, disciplineScore) =>
        discipline -> DisciplineStatistics(disciplineScore.totalSentenceYears / disciplineScore.entrants,
          disciplineScore.entrants,
          disciplineScore.confessions,
          disciplineScore.silents,
          disciplineScore.totalRuntime / (disciplineScore.entrants * interrogationsPerMatch))
    }.toSeq.sortBy(_._2.averageSentenceYears)

  private def convertToEntrantStatistics(rawEntrantScores: mutable.Map[Entrant, EntrantAccumulator]): Seq[(Entrant, EntrantStatistics)] = {
    rawEntrantScores.map {
      case (entrant, entrantScore) =>
        entrant -> EntrantStatistics(
          totalSentenceYears = entrantScore.totalSentenceYears,
          confessions = entrantScore.confessions,
          silents = entrantScore.silents,
          averageRuntime = entrantScore.totalRuntime / entrantScore.interrogationCount)
    }.toSeq.sortBy(_._2.totalSentenceYears)
  }

  private def accumulateDiscipline(disciplineAccumulators: mutable.Map[Discipline, DisciplineAccumulator],
                                   entrant: Entrant,
                                   entrantAccumulator: EntrantAccumulator): mutable.Map[Discipline, DisciplineAccumulator] = {
    if (!disciplineAccumulators.contains(entrant.discipline)) {
      disciplineAccumulators.put(entrant.discipline, DisciplineAccumulator())
    }
    val disciplineAccumulator = disciplineAccumulators(entrant.discipline)
    disciplineAccumulator.entrants += 1
    disciplineAccumulator.confessions += entrantAccumulator.confessions
    disciplineAccumulator.silents += entrantAccumulator.silents
    disciplineAccumulator.totalRuntime += entrantAccumulator.totalRuntime
    disciplineAccumulator.totalSentenceYears += entrantAccumulator.totalSentenceYears
    disciplineAccumulators
  }

  private def accumulateEntrants(entrantAccumulators: mutable.Map[Entrant, EntrantAccumulator],
                                 matchResult: MatchResult): mutable.Map[Entrant, EntrantAccumulator] = {
    def checkEntrantAccumulator(entrant: Entrant): EntrantAccumulator = {
      if (!entrantAccumulators.contains(entrant)) {
        entrantAccumulators.put(entrant, EntrantAccumulator())
      }
      entrantAccumulators(entrant)
    }

    val entrant1 = matchResult.matchUp.entrant1
    val entrant2 = matchResult.matchUp.entrant2
    val entrant1Accumulator = checkEntrantAccumulator(entrant1)
    val entrant2Accumulator = checkEntrantAccumulator(entrant2)

    accumulateInterrogations(matchResult, entrant1Accumulator, entrant2Accumulator)
    entrantAccumulators
  }

  private def accumulateInterrogations(matchResult: MatchResult, entrant1Accumulator: EntrantAccumulator, entrant2Accumulator: EntrantAccumulator): Unit = {
    val partnerAccumulators = Iterable(entrant1Accumulator, entrant2Accumulator)
    entrant1Accumulator.interrogationCount += matchResult.interrogations.size
    entrant2Accumulator.interrogationCount += matchResult.interrogations.size
    matchResult.interrogations.foreach { interrogation =>
      val response1 = interrogation.prisoner1Response.normalized
      val response2 = interrogation.prisoner2Response.normalized

      if (response1 == response2 && response1 == Silent) {
        partnerAccumulators.foreach {
          accumulateBothSilent
        }
      } else if (response1 == response2 && response1 == Confess) {
        partnerAccumulators.foreach {
          accumulateBothConfess
        }
      } else {
        accumulateSplit(entrant1Accumulator, entrant2Accumulator, response1)
      }

      entrant1Accumulator.totalRuntime += interrogation.prisoner1Time
      entrant2Accumulator.totalRuntime += interrogation.prisoner2Time
    }
  }

  private def accumulateBothSilent(accumulator: EntrantAccumulator): Unit = {
    accumulator.silents += 1
    accumulator.totalSentenceYears += bothSilentYears
  }

  private def accumulateBothConfess(accumulator: EntrantAccumulator): Unit = {
    accumulator.confessions += 1
    accumulator.totalSentenceYears += bothConfessYears
  }

  private def accumulateSplit(entrant1Accumulator: EntrantAccumulator,
                              entrant2Accumulator: EntrantAccumulator,
                              response1: Response): Unit = {
    val betrayer = if (response1 == Confess) entrant1Accumulator else entrant2Accumulator
    val betrayed = if (response1 == Confess) entrant2Accumulator else entrant1Accumulator
    betrayer.confessions += 1
    betrayer.totalSentenceYears += splitConfessorYears
    betrayed.silents += 1
    betrayed.totalSentenceYears += splitSilentYears
  }

}

object TournamentScoringService {

  /**
    * A mutable score object that is used to compute the score for a single entrant.
    */
  private case class EntrantAccumulator(var totalSentenceYears: Int = 0,
                                        var confessions: Int = 0,
                                        var silents: Int = 0,
                                        var totalRuntime: Long = 0,
                                        var interrogationCount: Int = 0)

  /**
    * A mutable score object that is used to compute the score for a discipline.
    */
  private case class DisciplineAccumulator(var totalSentenceYears: Int = 0,
                                           var entrants: Int = 0,
                                           var confessions: Int = 0,
                                           var silents: Int = 0,
                                           var totalRuntime: Long = 0)
}
