package com.nerdery.prisonersdilemma.tournament

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.nerdery.prisonersdilemma.tournament.actor.{MatchReportingActor, ScoringActor, TournamentActor}
import com.nerdery.prisonersdilemma.tournament.factory.{EntrantFactory, TournamentFactory}
import com.nerdery.prisonersdilemma.tournament.model.TournamentResult
import com.nerdery.prisonersdilemma.tournament.service.{TournamentScoringService, TournamentScoringService$}
import play.api.libs.json.Json

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Main application that runs the tournament
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
object App {

  def main(args: Array[String]): Unit = {
    val configProfile = args.headOption.getOrElse("development")
    val tournamentConfig = TournamentConfig.loadConfig(configProfile)
    implicit val timeout = Timeout(tournamentConfig.timeoutHours hour)
    implicit val system = ActorSystem("prisoners-dilemma")
    println {
      Json.prettyPrint {
        Json.toJson {
          runConfiguredTournament(tournamentConfig)
        }
      }
    }
    system.terminate
  }

  private def runConfiguredTournament(tournamentConfig: TournamentConfig)
                                     (implicit system: ActorSystem, timeout: Timeout): TournamentResult =
    Await.result(
      new EntrantFactory(tournamentConfig.entrants.submissionsFolder)
        .parseCsv(tournamentConfig.entrants.entrantsDataFile)
        .flatMap { entrants =>
          val tournamentFactory = new TournamentFactory(
            tournamentConfig.interrogations.countPerMatch,
            tournamentConfig.interrogations.timeoutSeconds)
          tournamentFactory.buildTournament(entrants)
        }.flatMap { tournament =>
          (buildTournamentActor(tournamentConfig, system) ? tournament).mapTo[TournamentResult]
        },
      tournamentConfig.timeoutHours hour)

  private def buildTournamentActor(tournamentConfig: TournamentConfig, system: ActorSystem): ActorRef = {
    val scoringActor = system.actorOf(Props(classOf[ScoringActor], buildScoringService(tournamentConfig)))
    val matchReportingActor = system.actorOf(Props(classOf[MatchReportingActor]))
    system.actorOf(Props(classOf[TournamentActor], scoringActor, matchReportingActor))
  }

  private def buildScoringService(tournamentConfig: TournamentConfig): TournamentScoringService = {
    val scorer = new TournamentScoringService(
      bothConfessYears = tournamentConfig.scoring.bothConfessYears,
      bothSilentYears = tournamentConfig.scoring.bothSilentYears,
      splitConfessorYears = tournamentConfig.scoring.splitConfessorYears,
      splitSilentYears = tournamentConfig.scoring.splitSilentYears)
    scorer
  }
}
