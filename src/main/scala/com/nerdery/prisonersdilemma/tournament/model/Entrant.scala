package com.nerdery.prisonersdilemma.tournament.model

import java.io.File

import play.api.libs.json.{JsString, Json, Writes}

/**
  * An entrant in the tournament
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class Entrant(username: String, discipline: Discipline, command: String, submissionFolder: File) {

  def disciplineName: String = discipline.name
}

object Entrant {

  implicit val fileFormat = new Writes[File] {
    def writes(file: File) = JsString(file.getAbsolutePath)
  }

  implicit val entrantFormat = Json.writes[Entrant]
}
