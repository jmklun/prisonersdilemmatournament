package com.nerdery.prisonersdilemma.tournament.model

import play.api.libs.json.{JsString, JsValue, Writes}

/**
  * A prisoner's response to an interrogation
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
sealed trait Response {
  def value: String
  def normalized: Response = Silent
}

case object Confess extends Response {
  override val value = "confess"
  override val normalized = this
}

case object Silent extends Response { override val value = "silent" }
case object Error extends Response { override val value = "error" }
case object Slow extends Response { override val value = "slow" }

object Response {

  implicit val tournamentResultFormat = new Writes[Response] {
    override def writes(response: Response): JsValue = JsString(response.value)
  }

  /**
    * When parsing a response string, "confess" yields Confess, and anything else yields Silent.
    *
    * @param rawResponse Interrogation response string to parse
    * @return Confess or Silent
    */
  def parseResponse(rawResponse: String): Response =
    rawResponse.trim match {
      case Confess.value => Confess
      case _ => Silent
    }
}
