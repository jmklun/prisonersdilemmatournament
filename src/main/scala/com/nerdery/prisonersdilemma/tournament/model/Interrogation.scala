package com.nerdery.prisonersdilemma.tournament.model

import play.api.libs.json.Json

/**
  * A single interrogation iteration
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class Interrogation(prisoner1Response: Response,
                         prisoner2Response: Response,
                         prisoner1Time: Long,
                         prisoner2Time: Long)

object Interrogation {
  implicit val interrogationFormat = Json.writes[Interrogation]
}
