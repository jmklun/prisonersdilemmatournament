package com.nerdery.prisonersdilemma.tournament.model

import java.time.LocalDateTime

import play.api.libs.json.Json

/**
  * A match result is the result of a series of interrogation iterations in a match.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class MatchResult(matchUp: Match,
                       interrogations: Iterable[Interrogation],
                       startTime: LocalDateTime,
                       endTime: LocalDateTime)

object MatchResult {
  implicit val matchResultFormat = Json.writes[MatchResult]
}
