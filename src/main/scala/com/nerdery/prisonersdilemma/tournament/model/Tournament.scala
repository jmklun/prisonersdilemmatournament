package com.nerdery.prisonersdilemma.tournament.model

/**
  * A tournament is a series of matches between entrants.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class Tournament(countPerMatch: Int, timeoutSeconds: Int, matches: Iterable[Match])
