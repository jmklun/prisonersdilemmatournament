package com.nerdery.prisonersdilemma.tournament.model

import java.time.LocalDateTime

import play.api.libs.json.Json

/**
  * A tournament result tracks the results of some or all the matches in a tournament.
  * If endTime is None, then the tournament isn't complete yet.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class TournamentResult(startTime: LocalDateTime, endTime: Option[LocalDateTime] = None, matches: Iterable[MatchResult])

object TournamentResult {
  implicit val tournamentResultFormat = Json.writes[TournamentResult]
}
