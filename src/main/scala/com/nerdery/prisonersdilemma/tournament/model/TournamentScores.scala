package com.nerdery.prisonersdilemma.tournament.model

import play.api.libs.json._

/**
  * A full scoring report for entrants and disciplines, with both ordered from least years sentenced to most.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class TournamentScores(entrantRankings: Iterable[(Entrant, EntrantStatistics)],
                            disciplineRankings: Iterable[(Discipline, DisciplineStatistics)])

case class EntrantStatistics(totalSentenceYears: Int,
                             confessions: Int,
                             silents: Int,
                             averageRuntime: Float)

case class DisciplineStatistics(averageSentenceYears: Float,
                                entrants: Int,
                                confessions: Int,
                                silents: Int,
                                averageRuntime: Float)

object TournamentScores {

  implicit val entrantStatisticsFormat = Json.writes[EntrantStatistics]
  implicit val disciplineStatisticsFormat = Json.writes[DisciplineStatistics]

  implicit val entrantRankingsFormat = new Writes[Iterable[(Entrant, EntrantStatistics)]] {
    override def writes(entrantRankings: Iterable[(Entrant, EntrantStatistics)]): JsValue =
      JsArray {
        entrantRankings.map {
          case (entrant, statistics) => Json.toJson(entrant).as[JsObject] ++ Json.toJson(statistics).as[JsObject]
        }.toSeq
      }
  }
  implicit val disciplineRankingsFormat = new Writes[Iterable[(Discipline, DisciplineStatistics)]] {
    override def writes(disciplineRankings: Iterable[(Discipline, DisciplineStatistics)]): JsValue =
      JsArray {
        disciplineRankings.map {
          case (discipline, statistics) =>
            JsObject(Seq("discipline" -> Json.toJson(discipline))) ++ Json.toJson(statistics).as[JsObject]
        }.toSeq
      }
  }

  implicit val tournamentScoresFormat = Json.writes[TournamentScores]
}


