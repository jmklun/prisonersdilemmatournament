package com.nerdery.prisonersdilemma.tournament.model

import play.api.libs.json.{JsString, Writes, Json}

/**
  * An enumeration of the allowed disciplines in the tournament
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
sealed trait Discipline {
  def name: String
}

case object ClientSideDiscipline extends Discipline { override val name = "client-side" }
case object DotNetDiscipline extends Discipline { override val name = ".net" }
case object JvmDiscipline extends Discipline { override val name = "jvm" }
case object MobileDiscipline extends Discipline { override val name = "mobile" }
case object PhpDiscipline extends Discipline { override val name = "php" }
case object RubyDiscipline extends Discipline { override val name = "ruby" }
case object OtherDiscipline extends Discipline { override val name = "other" }

object Discipline {

  implicit val disciplineFormat = new Writes[Discipline] {
    def writes(discipline: Discipline) = JsString(discipline.name)
  }
}
