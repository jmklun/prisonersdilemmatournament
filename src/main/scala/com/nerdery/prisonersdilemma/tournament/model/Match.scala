package com.nerdery.prisonersdilemma.tournament.model

import play.api.libs.json.Json

/**
  * A match is a competition between two entrants.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class Match(entrant1: Entrant, entrant2: Entrant)

object Match {
  implicit val matchFormat = Json.writes[Match]
}
