package com.nerdery.prisonersdilemma.tournament.factory

import java.io.{File, FileReader}

import com.github.tototoshi.csv.CSVReader
import com.nerdery.prisonersdilemma.tournament.model._
import com.typesafe.scalalogging.LazyLogging
import resource._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Factory to build Entrant objects from raw data.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class EntrantFactory(submissionRootFolder: File) extends LazyLogging {

  private val csvRowCount = 3

  def buildEntrant(username: String, discipline: String, command: String, submissionFolder: File): Entrant =
    Entrant(
      username,
      discipline match {
        case ClientSideDiscipline.name => ClientSideDiscipline
        case DotNetDiscipline.name => DotNetDiscipline
        case JvmDiscipline.name => JvmDiscipline
        case MobileDiscipline.name => MobileDiscipline
        case PhpDiscipline.name => PhpDiscipline
        case RubyDiscipline.name => RubyDiscipline
        case OtherDiscipline.name => OtherDiscipline
      },
      command,
      submissionFolder
    )

  /**
    * Asynchronously parse CSV in 'username,discipline,command' format.
    *
    * @param inputFile The CSV file to parse
    * @return A future that returns an iterable of Entrants for each valid row of the input file.
    */
  def parseCsv(inputFile: File): Future[Iterable[Entrant]] =
    Future {
      val resource = for (input <- managed(new FileReader(inputFile))) yield {
        CSVReader
          .open(input)
          .toStream
          .partition(_.length == csvRowCount) match {
          case (goodRows, badRows) =>
            badRows.foreach { badRow =>
              logger.warn(s"Row is formatted incorrectly and will be ignored: $badRow")
            }
            goodRows.map {
              case List(username, discipline, command) =>
                val submissionFolder = new File(submissionRootFolder, username)
                buildEntrant(
                  username = username,
                  discipline = discipline,
                  command = command,
                  submissionFolder = submissionFolder)
            }
        }
      }
      resource.opt.get
    }
}
