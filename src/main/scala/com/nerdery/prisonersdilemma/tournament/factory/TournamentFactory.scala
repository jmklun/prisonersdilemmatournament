package com.nerdery.prisonersdilemma.tournament.factory

import com.nerdery.prisonersdilemma.tournament.model.{Entrant, Match, Tournament}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * A factory that builds tournament objects with the configured interrogation (per match) count and the given interrogation timeout.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class TournamentFactory(countPerMatch: Int, timeoutSeconds: Int) {

  /**
    * Build a tournament with head-to-head matches for each of the given entrants.
    *
    * @return A future that return the newly created tournament object.
    */
  def buildTournament(entrants: Iterable[Entrant]): Future[Tournament] =
    Future {
      Tournament(
        countPerMatch = countPerMatch,
        timeoutSeconds = timeoutSeconds,
        matches = entrants
          .toSet
          .subsets(2) // 2 is the number of prisoners in a match.
          .map { entrantSet => Match(entrantSet.head, entrantSet.last) }
          .toIterable
      )
    }
}
