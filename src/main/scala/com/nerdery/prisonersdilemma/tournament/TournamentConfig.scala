package com.nerdery.prisonersdilemma.tournament

import java.io.File

import com.typesafe.config.ConfigFactory
import scala.concurrent.duration._
import scala.language.postfixOps

/**
  * Configuration object for a tournament.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
case class TournamentConfig(timeoutHours: Int,
                            interrogations: InterrogationsConfig,
                            entrants: EntrantsConfig,
                            scoring: ScoringConfig)

case class ScoringConfig(bothConfessYears: Int, bothSilentYears: Int, splitConfessorYears: Int, splitSilentYears: Int)

case class InterrogationsConfig(countPerMatch: Int,
                                timeoutSeconds: Int) {
  lazy val timeoutDuration = timeoutSeconds seconds
}

case class EntrantsConfig(entrantsDataPath: String, submissionPath: String) {
  def entrantsDataFile = new File(entrantsDataPath).getAbsoluteFile
  def submissionsFolder = new File(submissionPath).getAbsoluteFile
}

object TournamentConfig {

  /**
    * Load the configuration with the given profile name.
    *
    * @param profile The name of the profile (e.g. "development" or "production") to load
    * @return A new config object, populated with values from the given profile
    */
  def loadConfig(profile: String): TournamentConfig = {
    val tournamentConfig = ConfigFactory.load(profile).getConfig("tournament")
    val interrogationsConfig = tournamentConfig.getConfig("interrogations")
    val entrantsConfig = tournamentConfig.getConfig("entrants")
    val scoringConfig = tournamentConfig.getConfig("scoring")
    TournamentConfig(
      tournamentConfig.getInt("timeoutHours"),
      InterrogationsConfig(
        countPerMatch = interrogationsConfig.getInt("count"),
        timeoutSeconds = interrogationsConfig.getInt("timeoutSeconds")),
      EntrantsConfig(
        entrantsDataPath = entrantsConfig.getString("filename"),
        submissionPath = entrantsConfig.getString("submissionPath")),
      ScoringConfig(
        bothConfessYears = scoringConfig.getInt("bothConfessYears"),
        bothSilentYears = scoringConfig.getInt("bothSilentYears"),
        splitConfessorYears = scoringConfig.getInt("splitConfessorYears"),
        splitSilentYears = scoringConfig.getInt("splitSilentYears")))
  }
}
