package com.nerdery.prisonersdilemma.tournament.actor

import java.io.{File, FileWriter}
import java.nio.file.Files
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

import akka.actor.Actor
import com.github.tototoshi.csv.CSVWriter
import com.nerdery.prisonersdilemma.tournament.actor.MatchReportingActor.TournamentStartMessage
import com.nerdery.prisonersdilemma.tournament.model.{Interrogation, Match, MatchResult}
import resource._

/**
  * An actor that generates CSV reports for matches as they are completed.
  * Every time a TournamentStartMessage message is sent, the name of the CSV output file is
  * updated to reflect the tournament start time. Subsequent messages should be MatchResult.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class MatchReportingActor extends Actor {

  private lazy val headerBytes = "Match Start Time, Match End Time, Prisoner 1 Username, Prisoner 2 Username, Prisoner 1 Response, Prisoner 2 Response, Prisoner 1 Time (ms), Prisoner 2 Time (ms)\n".getBytes
  private lazy val fileDateFormatter = DateTimeFormatter.ofPattern("MM_dd_yyyy_kk_mm_ss")
  private var tournamentStartTime: Option[LocalDateTime] = None

  override def receive: Receive = {
    case TournamentStartMessage(newStartTime) =>
      updateStartTime(newStartTime)
    case MatchResult(matchUp, interrogations, matchStartTime, matchEndTime) =>
      reportMatch(matchUp, interrogations, matchStartTime, matchEndTime)
  }

  private def reportMatch(matchUp: Match,
                          interrogations: Iterable[Interrogation],
                          matchStartTime: LocalDateTime,
                          matchEndTime: LocalDateTime): Unit =
    for (output <- managed(new FileWriter(outputFile, true))) {
      val matchRowData = Seq(
        matchStartTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        matchEndTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
        matchUp.entrant1.username,
        matchUp.entrant2.username)
      CSVWriter
        .open(output)
        .writeAll {
          interrogations.map { interrogation =>
            matchRowData ++ Seq(
              interrogation.prisoner1Response,
              interrogation.prisoner2Response,
              interrogation.prisoner1Time,
              interrogation.prisoner2Time)
          }.toSeq
        }
    }

  private def updateStartTime(newStartTime: LocalDateTime): Unit = {
    tournamentStartTime = Some(newStartTime)
    Files.write(outputFile.toPath, headerBytes)
  }

  private def outputFile: File = {
    val fileDateString = tournamentStartTime.fold("invalid_start_time")(_.format(fileDateFormatter))
    new File(s"prisoners-dilemma-tournament-$fileDateString.csv")
  }
}

object MatchReportingActor {
  case class TournamentStartMessage(startTime: LocalDateTime)
}
