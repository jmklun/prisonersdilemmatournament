package com.nerdery.prisonersdilemma.tournament.actor

import java.io.IOException
import java.nio.file.{Files, Path}
import java.time.LocalDateTime
import java.util.concurrent.TimeoutException

import akka.actor.{ActorRef, Actor, ActorLogging}
import com.nerdery.prisonersdilemma.tournament.actor.MatchReportingActor.TournamentStartMessage
import com.nerdery.prisonersdilemma.tournament.model._
import org.apache.commons.io.FileUtils

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps
import scala.sys.process._

/**
  * TournamentActor runs a tournament and returns a tournament result.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class TournamentActor(scoringActor: ActorRef, matchReportingActor: ActorRef) extends Actor with ActorLogging {

  /**
    * Send a Tournament object to run the tournament asynchronously and return a TournamentResult to the sender.
    */
  override def receive: Receive = {
    case tournament: Tournament => sender ! runTournament(tournament)
  }

  /**
    * Run the tournament synchronously, returning results when completed.
    */
  def runTournament(tournament: Tournament): TournamentResult = {
    val startTime = LocalDateTime.now
    matchReportingActor ! TournamentStartMessage(startTime)
    val matchResults = runMatches(tournament)
    val endTime = LocalDateTime.now
    val result = TournamentResult(startTime, Some(endTime), matchResults)
    // TODO: Maybe create a system for sending intermediary TournamentResult objects off to the ScoringActor
    scoringActor ! result
    result
  }

  /**
    * Run the matches in the given tournament.
    */
  private def runMatches(tournament: Tournament): Iterable[MatchResult] =
  // TODO: This toArray is lame, but may be a workaround for a Scala bug with Set.subsets only iterating once on Map. Maybe file a bug.
    tournament.matches.toArray.map { matchUp =>
      val startTime = LocalDateTime.now
      val interrogations = runMatch(
        matchUp = matchUp,
        interrogationCount = tournament.countPerMatch,
        timeoutSeconds = tournament.timeoutSeconds)
      val endTime = LocalDateTime.now
      val matchResult = MatchResult(
        matchUp = matchUp,
        interrogations = interrogations,
        startTime = startTime,
        endTime = endTime)
      matchReportingActor ! matchResult
      matchResult
    }

  /**
    * Run a match between two prisoners with the given number of iterations and timeout.
    */
  private def runMatch(matchUp: Match, interrogationCount: Int, timeoutSeconds: Int): Iterable[Interrogation] = {
    var tempFolder1, tempFolder2: Path = null
    try {
      // Create temp folders.
      tempFolder1 = copySubmissionFolderToTemp(matchUp.entrant1)
      tempFolder2 = copySubmissionFolderToTemp(matchUp.entrant2)

      @tailrec
      def runInterrogations(remainingCount: Int, previousResponses: List[Interrogation] = List[Interrogation]()): List[Interrogation] = {
          val previousInterrogation = previousResponses.lastOption
          // This may all be a bit too clever. Mapping the entrants prevents repeated code, but has too many ugly tuple accesses.
          val entrantFolders = Map(
            matchUp.entrant1 -> (tempFolder1, previousInterrogation.map(_.prisoner1Response)),
            matchUp.entrant2 -> (tempFolder2, previousInterrogation.map(_.prisoner2Response)))
          val responses = entrantFolders.toIterable.map {
            case (entrant, (tempFolder, entrantResponse)) =>
              val startTime = System.currentTimeMillis
              val partner = entrantFolders.keys.filterNot(_ == entrant).head
              val partnerResponse = entrantFolders.filterNot(_._1 == entrant).head._2._2
              val response = getInterrogationResponse(
                entrant = entrant,
                partner = partner,
                entrantResponse = entrantResponse,
                partnerResponse = partnerResponse,
                timeoutSeconds = timeoutSeconds,
                tempFolder = tempFolder)
              response -> (System.currentTimeMillis - startTime)
          }
          val interrogation = Interrogation(
            prisoner1Response = responses.head._1,
            prisoner2Response = responses.last._1,
            prisoner1Time = responses.head._2,
            prisoner2Time = responses.last._2)
          val combinedResponses = previousResponses :+ interrogation
          if (remainingCount == 1) {
            combinedResponses
          } else {
            runInterrogations(remainingCount - 1, combinedResponses)
          }
      }

      runInterrogations(interrogationCount)
    } catch {
      case e: IOException =>
        // This is an error during the creation or removal of the temp folders, and means something went really wrong.
        log.error(e, s"Severe error running match: $matchUp")
        (0 to interrogationCount).map { i => Interrogation(Error, Error, 0, 0)}
    } finally {
      // Delete temp folders.
      def safeDelete(tempFolder: Path) = if (tempFolder != null && tempFolder.toFile.exists) {
        FileUtils.deleteDirectory(tempFolder.toFile)
      }
      Iterable(tempFolder1, tempFolder2).foreach { safeDelete }
    }
  }

  /**
    * Copy the entrant's submission folder to a temp location to be executed. This ensures that the submissions aren't damaged
    * and that the don't carry state from one match to the next, but can between iterations in a match.
    *
    * @param entrant The entrant whose submission should be copied
    * @return The path to the temp folder
    */
  private def copySubmissionFolderToTemp(entrant: Entrant): Path = {
    val tempFolder = Files.createTempDirectory("prisoners")
    val sourcePath = entrant.submissionFolder.getAbsolutePath
    val destinationPath = tempFolder.toAbsolutePath
    // Originally, this code used Apache's copy, but that doesn't maintain permissions properly:
    // FileUtils.copyDirectory(entrant.submissionFolder, tempFolder.toFile)
    // This implementation is Mac OS specific and will fail on other platforms.
    val resultCode = s"ditto $sourcePath $destinationPath".!
    if (resultCode != 0) {
      throw new IOException(s"Failed to copy from $sourcePath to $destinationPath.")
    }
    tempFolder
  }

  private def getInterrogationResponse(entrant: Entrant,
                                       partner: Entrant,
                                       entrantResponse: Option[Response],
                                       partnerResponse: Option[Response],
                                       timeoutSeconds: Int,
                                       tempFolder: Path): Response =
    try {
      val commandFuture = runInterrogationCommand(
        entrant = entrant,
        partner = partner,
        entrantResponse = entrantResponse,
        partnerResponse = partnerResponse,
        workingFolder = tempFolder)
      // Running the command in a future, then forcing it to be synchronous keeps execution serial, but allows an easy timeout mechanism.
      val interrogationResult = Await.result(commandFuture, timeoutSeconds seconds)
      Response.parseResponse(interrogationResult)
    } catch {
      case e: TimeoutException =>
        log.warning(s"Entrant [${entrant.username}] did not complete in time. Response [Slow] will be used.")
        Slow
      case e: Exception =>
        log.error(e, s"Entrant [${entrant.username}] encountered an error during execution. Response [Error] will be used.")
        Error
    }

  /**
    * Asynchronously run the interrogation command for an entrant.
    *
    * @param entrant The prisoner who is being interrogated
    * @param partner The partner who is not being interrogated
    * @param entrantResponse The previous response from entrant
    * @param partnerResponse The previous response from partner
    * @param workingFolder The temporary working folder where the interrogation command will be executed
    * @return A future for the string output of the command
    */
  private def runInterrogationCommand(entrant: Entrant,
                                      partner: Entrant,
                                      entrantResponse: Option[Response],
                                      partnerResponse: Option[Response],
                                      workingFolder: Path): Future[String] =
    Future[String] {
      def responseOptToArg(responseOpt: Option[Response]): String = responseOpt.fold("")(_.normalized.value)
      val interrogationCommand = s"${entrant.command} ${partner.username} ${partner.disciplineName} ${responseOptToArg(partnerResponse)} ${responseOptToArg(entrantResponse)}"
      Process(interrogationCommand, Some(workingFolder.toFile)).!!
    }
}
