package com.nerdery.prisonersdilemma.tournament.actor

import akka.actor.{ActorLogging, Actor}
import com.nerdery.prisonersdilemma.tournament.model.{TournamentScores, TournamentResult}
import com.nerdery.prisonersdilemma.tournament.service.TournamentScoringService
import play.api.libs.json.Json

/**
  * An actor that performs asynchronous scoring operations.
  *
  * @author Josh Klun (jklun@nerdery.com)
  */
class ScoringActor(scorer: TournamentScoringService) extends Actor with ActorLogging {

  private def scoreTournament(tournamentResult: TournamentResult): TournamentScores = scorer.scoreTournament(tournamentResult)

  override def receive: Receive = {
    case tournamentResult: TournamentResult =>
      val scores = scoreTournament(tournamentResult)
      println(Json.prettyPrint(Json.toJson(scores)))
  }
}
